/*********************************************************
*                                                        *
*   PrimeSense NiTE 2.0 - User Viewer Sample             * 
*   Copyright (C) 2012 PrimeSense Ltd.                   *
*                                                        *
*********************************************************/

#include "kinect_v2/viewer2.h"

int main(int argc, char** argv)
{
  openni::Status rc = openni::STATUS_OK;

  Viewer2 Viewer2("Viewer printing data");

  rc = Viewer2.Init(argc, argv);
  if (rc != openni::STATUS_OK)
  {
    return 1;
  }
  Viewer2.Run();
}
