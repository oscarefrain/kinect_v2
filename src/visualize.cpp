
#include "ros/ros.h"
#include <Eigen/Dense>

#include <kinect_v2/markers.hpp>
#include <kinect_v2/viewer2.h>


int main(int argc, char **argv)
{
  ros::init(argc, argv, "visualize");
  ros::NodeHandle n;

  openni::Status rc = openni::STATUS_OK;

  double color_green[3] = {0.0, 1.0, 0.0};
  Viewer2 Viewer2("Viewer printing data", n, color_green);

  rc = Viewer2.Init(argc, argv);
  if (rc != openni::STATUS_OK)
  {
    return 1;
  }

  // BallMarker m_left_hand2(n, color_green);
  // BallMarker m_left_elbow2(n, color_green);
  // BallMarker m_left_shoulder2(n, color_green);

  // Eigen::VectorXd pleft_hand(3), pleft_elbow(3), pleft_shoulder(3);
  // pleft_hand << 0, 0, 0.5;
  // pleft_elbow << 0, 0.5, 0.5;
  // pleft_shoulder << 0, -0.5, 0.5;
  
  // m_left_hand.setPose(pleft_hand);
  // m_left_elbow.setPose(pleft_elbow);
  // m_left_shoulder.setPose(pleft_shoulder);

  unsigned int cnt = 1;
  ros::Rate rate(10);
  while (ros::ok())
  {
    if (cnt == 1)
    {
      Viewer2.Run();
      cnt = 0;
    }
    // m_left_hand.publish();
    // m_left_elbow.publish();
    // m_left_shoulder.publish();

    ros::spinOnce();
    rate.sleep();
  }

  return 0;
}
