/*********************************************************
*                                                        *
*   PrimeSense NiTE 2.0 - User Viewer Sample             * 
*   Copyright (C) 2012 PrimeSense Ltd.                   *
*                                                        *
*********************************************************/

#ifndef _VIEWER2_H_
#define _VIEWER2_H_

#include "NiTE.h"
#include <kinect_v2/markers.hpp>
#include "ros/ros.h"

#define MAX_DEPTH 10000

class Viewer2
{
public:
  Viewer2(const char* strName, ros::NodeHandle& nh, double color[3]);
  virtual ~Viewer2();

  // Initialize the tracker
  virtual openni::Status Init(int argc, char **argv);
  virtual openni::Status Run();	//Does not return

protected:
  virtual void Display();
  // Not in Viewer.cpp
  virtual void DisplayPostDraw(){};	// Overload to draw over the screen image
  virtual void OnKey(unsigned char key, int x, int y);
  virtual openni::Status InitOpenGL(int argc, char **argv);
  void InitOpenGLHooks();
  void Finalize();

private:
  Viewer2(const Viewer2&);
  Viewer2& operator=(Viewer2&);

  static Viewer2* self_;
  static void glutIdle();
  static void glutDisplay();
  static void glutKeyboard(unsigned char key, int x, int y);

  // void show_markers();
  
  float                pDepthHist_[MAX_DEPTH];
  char                 strName_[ONI_MAX_STR];
  // Size of texture map
  openni::RGB888Pixel* pTexMap_;
  unsigned int         nTexMapX_;
  unsigned int         nTexMapY_;

  openni::Device       device_;
  // Main Object of User Tracker algorithm: provides access to skeleton
  nite::UserTracker* userTrackerPtr_;

  nite::UserId poseUser_;
  uint64_t poseTime_;

  // ROS node handler
  ros::NodeHandle nh_;

  // Default color
  // double color_green_[3] = {0.0, 1.0, 0.0};

  // Viewers
  BallMarker m_left_hand_;
  BallMarker m_left_elbow_;
  BallMarker m_left_shoulder_;

};


#endif // _NITE_USER_VIEWER_H_
